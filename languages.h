struct language {
    char *name;
    char *template;
} languages[] = {
    {
        "c",
        "#include <stdio.h>\n"
        "#include <stdlib.h>\n"
        "\n"
        "int main(int argc, char *argv[]) {\n"
        "    <++>\n"
        "\n"
        "    return 0;\n"
        "}"
    },
    {
        "cpp",
        "#include <iostream>\n"
        "\n"
        "using namespace std;\n"
        "\n"
        "int main(int argc, char *argv[]) {\n"
        "    <++>\n"
        "\n"
        "    return 0;\n"
        "}"
    },
    {
        "shell",
        "#!/bin/sh\n"
        "\n"
        "<++>"
    },
    {
        "tex",
        "\\documentclass{article}\n"
        "\n"
        "\\begin{document}\n"
        "\n"
        "<++>\n"
        "\n"
        "\\end{document}"
    },
    {
        "html",
        "<!DOCTYPE HTML>\n"
        "<html>\n"
        "<head>\n"
        "    <meta charset=\"UTF-8\">\n"
        "    <title><++></title>\n"
        "</head>\n"
        "<body>\n"
        "    <++>\n"
        "</body>\n"
        "</html>"
    }
};
