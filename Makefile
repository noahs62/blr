CC = gcc
CFLAGS = -Werror -Wall -Wextra -pedantic-errors -std=c99
OUTFILE = blr
OBJS = main.c

all: $(OBJS)
	$(CC) $(CFLAGS) -o $(OUTFILE) $(OBJS)

install: $(OBJS)
	$(CC) $(CFLAGS) -o $(OUTFILE) $(OBJS)
	install -g 0 -o 0 $(OUTFILE) /usr/local/bin/
	install -g 0 -o 0 -m 0644 blr.1 /usr/share/man/man1/
	gzip -f /usr/share/man/man1/blr.1
